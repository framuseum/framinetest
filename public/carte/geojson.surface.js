var surfaceCollection = {
  "type": "FeatureCollection",
  "features": [{
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Temple de l'eau</b>, <br/><i>Charlie</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-1000, 135]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Temple</b>, <br/><i>Mijak</i><br/><img src='captures/EgliseMijak.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [118, -1032]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Zone de quizz</b>, <br/><i>powi & Peppy</i><br/><img src='captures/ZoneQuizz.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [1200, 1160]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Belle géné</b>, <br/><i>gene</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [1275, 171]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Jolie maison d'archi</b>, <br/><i>??</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [1343, -306]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Petite cabane</b>, <br/><i>??</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [1547, -372]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Maison fantôme</b>, <br/><i>talou</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-164, -384]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Kyosque sur ilot</b>, <br/><i>??</i><br/><img src='captures/KyosqueIlot.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [1643, -386]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Maison</b>, <br/><i>talou</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-175, 18]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Arbre géant</b>, <br/><i>talou</i><br/><img src='captures/ArbreGeantTalou.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [182, -42]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Palais licorne</b>, <br/><i>Simeon6C & Dorian6C</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-1900, -300]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Chantier d'un campus universitaire</b>, <br/><i>Classe de Beurt</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-1964, 651]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Tour de babel</b>, <br/><i>talou</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [2145, 1886]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Scène de mariage</b>, <br/><i>??</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [264, -203]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Tour Eiffel</b>, <br/><i>Lilou</i><br/><img src='captures/TourEiffel.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [331, 308]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Pyramides</b>, <br/><i>DooM</i><br/><img src='captures/PyramidesDoom.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [5200, -100]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Stade</b>, <br/><i>Lilou</i><br/><img src='captures/StadeLilou.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-358, -186]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Taverne du géant endormi</b>, <br/><i>Mijak</i><br/><img src='captures/TaverneGeantMijak.png' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [368, -558]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Fabrique à objets</b>, <br/><i>??</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-373, -126]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Grotte scarabée</b>, <br/><i>Dwayn</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-400, -256]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Tunnel routier</b>, <br/><i>GW</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [400, -50]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Yin & Yang</b>, <br/><i>powi</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [360, -90]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Arbre magique</b>, <br/><i>Peppy</i><br/><img src='captures/ArbreMagiquePeppy.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-7, -123]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Cabane</b>, <br/><i>Mijak</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [422, -196]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Abris dans les arbres</b>, <br/><i>gene</i><br/><img src='captures/AbrisArbresGene.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-435, 26]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Géants de laine</b>, <br/><i>farfadet46</i><br/><img src='captures/GeantsFarfadet.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [55, -200]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Ovni</b>, <br/><i>Ichdvdazh</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [469, -313]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Chateau qui rend fou</b>, <br/><i>Sangokuss</i><br/><img src='captures/ChateauSangokuss.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-500, -200]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Manoir</b>, <br/><i>Mijak</i><br/><img src='captures/ManoirMijak.png' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [527, -645]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Hermitage du desert</b>, <br/><i>Talou</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-557, 1874]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Amphi</b>, <br/><i>Dwayn & qwerty</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-565, 553]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Spawn</b>, <br/><i>Andrey01 & farfadet46</i><br/><img src='captures/Spawn.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-60, -120]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Yacht</b>, <br/><i>Gee</i><br/><img src='captures/YachtGee.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-666, -114]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Cabane dans l'arbre</b>, <br/><i>??</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [669, -106]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Bibliotheque du goulag</b>, <br/><i>Dwayn</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-700, 741]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Batiment</b>, <br/><i>qwerty</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-716, 517]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Joli pont</b>, <br/><i>??</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [7, -185]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Aéroport</b>, <br/><i>GW</i><br/><img src='captures/AeroportGW.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [685, -671]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Ampli géant</b>, <br/><i>Peppy</i><br/><img src='captures/AmpliPeppy.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [764, -88]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b> Resort</b>, <br/><i>GW</i><br/><img src='captures/GWResort.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [829, -600]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Manoir</b>, <br/><i>??</i><br/><img src='captures/ManoirDePierre.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [833, 855]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Maison</b>, <br/><i>Luka</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [864, -536]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Maison</b>, <br/><i>murr45s</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [233, -261]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>EntréeDemeure de PéhA</b>, <br/><i>peha</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [247, -174]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Musée des consoles</b>, <br/><i>peha</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [325, -257]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Maison dans les arbres</b>, <br/><i>??</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [14, -97]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Centre de téléportation</b>, <br/><i>powi</i><br/><img src='captures/TeleportHub.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-135, -219]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Megaplongeoir</b>, <br/><i>??</i><br/><img src='captures/Megaplongeoir.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-436, 116]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Goulag</b>, <br/><i>Dwayn</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-700, 733]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Grande bibliothèque</b>, <br/><i>??</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-597, 536]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Framaduc</b>, <br/><i>Mijak</i><br/><img src='captures/FramaducMijak.png' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [168, -435]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Ziggurat of Doom</b>, <br/><i>Luc</i><br/><img src='captures/TempleOfDoom.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-718, -127]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Village du libre</b>, <br/><i>Bookynette</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [27, 1983]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Zone de non-droit</b>, <br/><i>powi</i><br/><img src='captures/ZoneNonDroit.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-550, -550]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Maison</b>, <br/><i>Oberon</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-153, -141]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Hôtel de ville</b>, <br/><i>Simeon6C</i><br/><img src='captures/hdv.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-66, -53]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Hôtel</b>, <br/><i>farfadet46</i><br/><img src='captures/HotelFarfadet.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-248, -44]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Chateau d'eau</b>, <br/><i>??</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-3, -244]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Maison</b>, <br/><i>Titi_Alone</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [20, -273]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Datacenter du serveur</b>, <br/><i>??</i><br/><img src='captures/DataCenterServeur.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [52, -257]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Petit jardin serre</b>, <br/><i>Mityjvyt</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [38, -235]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Maisonnette du nain</b>, <br/><i>??</i><br/><img src='captures/MaisonNain.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-51, -263]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Maison</b>, <br/><i>farfadet46</i><br/><img src='captures/MaisonFarfadet.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-95, -217]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Maison</b>, <br/><i>sexybabe</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-91, -174]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Maison</b>, <br/><i>Morgane</i><br/><img src='captures/MaisonMorgane.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-78, 152]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Expérimentations électriques</b>, <br/><i>powi</i><br/><img src='captures/ExperimentationsMese.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-330, -270]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Auberge - Taverne - Gite et Salle de danse</b>, <br/><i>??</i><br/><img src='captures/AubergeGitesSalle.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [617, -212]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Petit pont de bois</b>, <br/><i>??</i><br/><img src='captures/PetitPontDeBois.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [560, 20]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Maison en or</b>, <br/><i>??</i><br/><img src='captures/MaisonEnOr.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [254, 120]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Cabanes</b>, <br/><i>dusehuz</i><br/><img src='captures/CabanesDusehuz.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [126, 77]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Maison</b>, <br/><i>katya</i><br/><img src='captures/MaisonKatya.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [9, 215]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Maison drôle de forme</b>, <br/><i>??</i><br/><img src='captures/MaisonDroleDeForme.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-124, 260]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Tetris géant</b>, <br/><i>powi</i><br/><img src='captures/TetrisGeant.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-312, 200]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Maison aux vaches</b>, <br/><i>??</i><br/><img src='captures/MaisonVaches.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [35, -311]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Tente de l'explorateur</b>, <br/><i>??</i><br/><img src='captures/TenteExplorateur.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [41, -285]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Temple du feu</b>, <br/><i>??</i><br/><img src='captures/TempleDuFeu.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [49, -328]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Maison</b>, <br/><i>Gabi</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-546, -430]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Maison</b>, <br/><i>coxe</i><br/><img src='captures/MaisonDeCoxe.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-133, -365]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Maisons aériennes</b>, <br/><i>powi & Fox1000</i><br/><img src='captures/MaisonsAeriennesFox1000.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [124, -924]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Maison</b>, <br/><i>foxcraft</i><br/><img src='captures/MaisonFoxcraft.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-116, -441]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Maison</b>, <br/><i>DooM</i><br/><img src='captures/MaisonDoom.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-150, 40]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Dojo</b>, <br/><i>talou</i><br/><img src='captures/DojoTalou.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-394, -126]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Maison</b>, <br/><i>Kinou</i><br/><img src='captures/MaisonKinou.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-623, -32]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Point d'interrogation</b>, <br/><i>??</i><br/><img src='captures/PointInterrogation.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-546, -75]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Maison</b>, <br/><i>gui</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [63, -123]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Maison</b>, <br/><i>Gee</i><br/><img src='captures/MaisonGee.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-824, -383]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Maison de compagne</b>, <br/><i>Sangokuss</i><br/><img src='captures/MaisonCompagneSango.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-720, -253]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Grande cheminée</b>, <br/><i>??</i><br/><img src='captures/GrandeCheminee.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [23, 16]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Deux droles de trous</b>, <br/><i>gene</i>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [2233, -138]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Drapeau trans non binaire</b>, <br/><i>Titi_Alone</i><br/><img src='captures/DrapeauTransNonBinaireTiti.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [270, -509]
      }
    },
    {
      "type": "Feature",
      "properties": {
        "popupContent": "<b>Maison</b>, <br/><i>Darth</i><br/><img src='captures/MaisonDarth.jpg' style='width:200px;'/>"
      },
      "geometry": {
        "type": "Point",
        "coordinates": [-310, -180]
      }
    }
  ]
};
